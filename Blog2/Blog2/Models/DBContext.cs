﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog2.Models
{
    public class DBContext :DbContext
    {
        public DbSet<Post> Posts { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Tags> Tags { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>()
                .HasMany(a => a.Tags).WithMany(s => s.Posts)
                .Map(d =>
                {
                    d.MapLeftKey("PostID");
                    d.MapRightKey("TagID");
                    d.ToTable("Post_Tag");
                });
        }
    }
}