﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2.Models
{
    public class Tags
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Không được để trống.")]
        [StringLength(100, ErrorMessage = "Độ dài tối thiểu từ 10-100 kí tự.", MinimumLength = 10)]
        public string Content { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}