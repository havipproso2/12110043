﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2.Models
{
    public class Account
    {
        public int AccountID { get; set; }

        [Required(ErrorMessage="Không được để trống.")]
        [StringLength(int.MaxValue,ErrorMessage="Mật khẩu chứa ít nhất 6 kí tự,phân biệt cả chữ HOA và chữ thường.",MinimumLength=6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage="Không được để trống.")]
        [EmailAddress(ErrorMessage="Nhập đúng định dạng email.")]
        public string Email { get; set; }

        [Required(ErrorMessage="Không được để trống")]
        [StringLength(100,ErrorMessage="Tối đa chỉ chứa 100 kí tự.")]
        public string FirtstName { get; set; }

        [Required(ErrorMessage="Không được để trống.")]
        [StringLength(100,ErrorMessage="Tối đa chỉ chứa 100 kí tự.")]
        public string LastName { get; set; }

        
        public virtual ICollection<Post> Posts { get; set; }
    }
}