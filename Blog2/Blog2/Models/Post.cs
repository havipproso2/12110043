﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2.Models
{
    public class Post
    {
        public int ID { get; set; }

        [Required(ErrorMessage="Không được để trống.")]
        [StringLength(500,ErrorMessage="Đồ dài tiêu chuẩn 20-500.",MinimumLength=20)]
        public string Title { get; set; }

        [Required(ErrorMessage="Không được để trống.")]
        [StringLength(int.MaxValue,ErrorMessage="Độ dài kí tự tối thiểu là 50 từ.",MinimumLength=50)]
        public string Body { get; set; }

        [Required(ErrorMessage="Không được bỏ trống.")]
        [DataType(DataType.DateTime,ErrorMessage="Nhập đúng kiểu dữ liệu.")]
        public DateTime DateCreated { get; set; }

        [Required(ErrorMessage = "Không được bỏ trống.")]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập đúng kiểu dữ liệu.")]
        public DateTime DateUpdated { get; set; }

        public int AccountId { get; set; }

        public virtual Account Accounts { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Tags> Tags { get; set; }
    }
}