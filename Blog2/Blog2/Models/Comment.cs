﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2.Models
{
    public class Comment
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Không được để trống.")]
        [StringLength(int.MaxValue, ErrorMessage = "Độ dài kí tự tối thiểu là 50 từ.", MinimumLength = 50)]
        public string Body { get; set; }

        [Required(ErrorMessage = "Không được bỏ trống.")]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập đúng kiểu dữ liệu.")]
        public DateTime DataCreated { get; set; }

        [Required(ErrorMessage = "Không được bỏ trống.")]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập đúng kiểu dữ liệu.")]
        public DateTime DataUpdated { get; set; }

        [Required(ErrorMessage = "Không được bỏ trống.")]
        public string Author { get; set; }

        public int PostID { get; set; }
        public virtual Post Posts{get;set;}
    }
}