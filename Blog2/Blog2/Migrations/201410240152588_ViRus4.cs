namespace Blog2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ViRus4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Posts", "DateCreated", c => c.DateTime(nullable: false));
            AddColumn("dbo.Posts", "DateUpdated", c => c.DateTime(nullable: false));
            DropColumn("dbo.Posts", "DataCreated");
            DropColumn("dbo.Posts", "DataUpdated");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Posts", "DataUpdated", c => c.DateTime(nullable: false));
            AddColumn("dbo.Posts", "DataCreated", c => c.DateTime(nullable: false));
            DropColumn("dbo.Posts", "DateUpdated");
            DropColumn("dbo.Posts", "DateCreated");
        }
    }
}
