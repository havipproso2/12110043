﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaiBlog.Models
{
    public class Comment
    {
        public int ID { get; set; }
        public string Body { get; set; }
    }
}