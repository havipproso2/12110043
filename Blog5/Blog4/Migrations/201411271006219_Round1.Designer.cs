// <auto-generated />
namespace Blog4.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class Round1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Round1));
        
        string IMigrationMetadata.Id
        {
            get { return "201411271006219_Round1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
