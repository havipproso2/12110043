namespace Blog4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Round3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Posts", "UserProfile_UserId", "dbo.UserProfile");
            DropIndex("dbo.Posts", new[] { "UserProfile_UserId" });
            RenameColumn(table: "dbo.Posts", name: "UserProfile_UserId", newName: "UserProfileUserID");
            AddForeignKey("dbo.Posts", "UserProfileUserID", "dbo.UserProfile", "UserId", cascadeDelete: true);
            CreateIndex("dbo.Posts", "UserProfileUserID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Posts", new[] { "UserProfileUserID" });
            DropForeignKey("dbo.Posts", "UserProfileUserID", "dbo.UserProfile");
            RenameColumn(table: "dbo.Posts", name: "UserProfileUserID", newName: "UserProfile_UserId");
            CreateIndex("dbo.Posts", "UserProfile_UserId");
            AddForeignKey("dbo.Posts", "UserProfile_UserId", "dbo.UserProfile", "UserId");
        }
    }
}
