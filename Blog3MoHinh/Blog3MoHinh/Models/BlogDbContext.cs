﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog3MoHinh.Models
{
    public class BlogDbContext :DbContext
    {
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Level> Levels { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Rate> Rates { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>()
                .HasMany(a => a.Admins).WithMany(s => s.Users)
                .Map(d =>
                {
                    d.MapLeftKey("UserID");
                    d.MapRightKey("AdminID");
                    d.ToTable("User_Admin");
                });
            modelBuilder.Entity<Post>()
                .HasMany(a => a.Tags).WithMany(s => s.Posts)
                .Map(d =>
                {
                    d.MapLeftKey("PostID");
                    d.MapRightKey("TagID");
                    d.ToTable("Post_Tag");
                });
        }
    }
}