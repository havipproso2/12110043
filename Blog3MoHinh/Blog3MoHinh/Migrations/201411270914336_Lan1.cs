namespace Blog3MoHinh.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Admins",
                c => new
                    {
                        AdminID = c.Int(nullable: false, identity: true),
                        AdminName = c.String(),
                        Password = c.String(),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AdminID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                        Level = c.String(),
                        Picture = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        Messeges = c.String(),
                        Slogan = c.String(),
                        NumberOfPosts = c.Int(nullable: false),
                        Levels_LevelID = c.Int(),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Levels", t => t.Levels_LevelID)
                .Index(t => t.Levels_LevelID);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        PostID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        Like = c.Int(nullable: false),
                        View = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PostID)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        CommentID = c.Int(nullable: false, identity: true),
                        PostId = c.Int(nullable: false),
                        Content = c.String(),
                    })
                .PrimaryKey(t => t.CommentID)
                .ForeignKey("dbo.Posts", t => t.PostId, cascadeDelete: true)
                .Index(t => t.PostId);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                        PostId = c.Int(nullable: false),
                        Content = c.String(),
                    })
                .PrimaryKey(t => t.TagID);
            
            CreateTable(
                "dbo.Rates",
                c => new
                    {
                        RateID = c.Int(nullable: false, identity: true),
                        PostID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RateID)
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .Index(t => t.PostID);
            
            CreateTable(
                "dbo.Levels",
                c => new
                    {
                        LevelID = c.Int(nullable: false, identity: true),
                        LevelName = c.String(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.LevelID);
            
            CreateTable(
                "dbo.User_Admin",
                c => new
                    {
                        UserID = c.Int(nullable: false),
                        AdminID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserID, t.AdminID })
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .ForeignKey("dbo.Admins", t => t.AdminID, cascadeDelete: true)
                .Index(t => t.UserID)
                .Index(t => t.AdminID);
            
            CreateTable(
                "dbo.Post_Tag",
                c => new
                    {
                        PostID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostID, t.TagID })
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.PostID)
                .Index(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Post_Tag", new[] { "TagID" });
            DropIndex("dbo.Post_Tag", new[] { "PostID" });
            DropIndex("dbo.User_Admin", new[] { "AdminID" });
            DropIndex("dbo.User_Admin", new[] { "UserID" });
            DropIndex("dbo.Rates", new[] { "PostID" });
            DropIndex("dbo.Comments", new[] { "PostId" });
            DropIndex("dbo.Posts", new[] { "UserID" });
            DropIndex("dbo.Users", new[] { "Levels_LevelID" });
            DropForeignKey("dbo.Post_Tag", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Post_Tag", "PostID", "dbo.Posts");
            DropForeignKey("dbo.User_Admin", "AdminID", "dbo.Admins");
            DropForeignKey("dbo.User_Admin", "UserID", "dbo.Users");
            DropForeignKey("dbo.Rates", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Comments", "PostId", "dbo.Posts");
            DropForeignKey("dbo.Posts", "UserID", "dbo.Users");
            DropForeignKey("dbo.Users", "Levels_LevelID", "dbo.Levels");
            DropTable("dbo.Post_Tag");
            DropTable("dbo.User_Admin");
            DropTable("dbo.Levels");
            DropTable("dbo.Rates");
            DropTable("dbo.Tags");
            DropTable("dbo.Comments");
            DropTable("dbo.Posts");
            DropTable("dbo.Users");
            DropTable("dbo.Admins");
        }
    }
}
