﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog3MoHinh.Models;

namespace Blog3MoHinh.Controllers
{
    public class RateController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /Rate/

        public ActionResult Index()
        {
            var rates = db.Rates.Include(r => r.Post);
            return View(rates.ToList());
        }

        //
        // GET: /Rate/Details/5

        public ActionResult Details(int id = 0)
        {
            Rate rate = db.Rates.Find(id);
            if (rate == null)
            {
                return HttpNotFound();
            }
            return View(rate);
        }

        //
        // GET: /Rate/Create

        public ActionResult Create()
        {
            ViewBag.PostID = new SelectList(db.Posts, "PostID", "PostID");
            return View();
        }

        //
        // POST: /Rate/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Rate rate)
        {
            if (ModelState.IsValid)
            {
                db.Rates.Add(rate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PostID = new SelectList(db.Posts, "PostID", "PostID", rate.PostID);
            return View(rate);
        }

        //
        // GET: /Rate/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Rate rate = db.Rates.Find(id);
            if (rate == null)
            {
                return HttpNotFound();
            }
            ViewBag.PostID = new SelectList(db.Posts, "PostID", "PostID", rate.PostID);
            return View(rate);
        }

        //
        // POST: /Rate/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Rate rate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PostID = new SelectList(db.Posts, "PostID", "PostID", rate.PostID);
            return View(rate);
        }

        //
        // GET: /Rate/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Rate rate = db.Rates.Find(id);
            if (rate == null)
            {
                return HttpNotFound();
            }
            return View(rate);
        }

        //
        // POST: /Rate/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Rate rate = db.Rates.Find(id);
            db.Rates.Remove(rate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}