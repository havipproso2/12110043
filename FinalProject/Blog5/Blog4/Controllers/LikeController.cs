﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog4.Models;

namespace Blog4.Controllers
{
    public class LikeController : Controller
    {
        private BlogContext db = new BlogContext();

        //
        // GET: /Like/

        public ActionResult Index()
        {
            var likes = db.Likes.Include(l => l.Post).Include(l => l.UserProfile);
            return View(likes.ToList());
        }

        //
        // GET: /Like/Details/5

        public ActionResult Details(int id = 0)
        {
            Like like = db.Likes.Find(id);
            if (like == null)
            {
                return HttpNotFound();
            }
            return View(like);
        }

        //
        // GET: /Like/Create

        public ActionResult Create()
        {
            ViewBag.PostID = new SelectList(db.Posts, "ID", "Title");
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Like/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Like like)
        {
            if (ModelState.IsValid)
            {
                db.Likes.Add(like);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PostID = new SelectList(db.Posts, "ID", "Title", like.PostID);
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", like.UserProfileUserID);
            return View(like);
        }

        //
        // GET: /Like/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Like like = db.Likes.Find(id);
            if (like == null)
            {
                return HttpNotFound();
            }
            ViewBag.PostID = new SelectList(db.Posts, "ID", "Title", like.PostID);
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", like.UserProfileUserID);
            return View(like);
        }

        //
        // POST: /Like/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Like like)
        {
            if (ModelState.IsValid)
            {
                db.Entry(like).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PostID = new SelectList(db.Posts, "ID", "Title", like.PostID);
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", like.UserProfileUserID);
            return View(like);
        }

        //
        // GET: /Like/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Like like = db.Likes.Find(id);
            if (like == null)
            {
                return HttpNotFound();
            }
            return View(like);
        }

        //
        // POST: /Like/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Like like = db.Likes.Find(id);
            db.Likes.Remove(like);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}