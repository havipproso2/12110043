﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog4.Models
{
    public class Level
    {
        public int LevelID { get; set; }
        public String TenCapBac { get; set; }
        public int SoBaiDang { get; set; }
        public string HinhAnh { get; set; }

        public virtual ICollection<UserProfile> UserProfiles { get; set; }
    }
}