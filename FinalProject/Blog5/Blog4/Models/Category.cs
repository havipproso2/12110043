﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog4.Models
{
    public class Category
    {
        public int CategoryID { get; set; }
        public String CategoryName { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}