﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog4.Models
{
    public class Comment
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public String Body { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        public String Author { get; set; }
        public String LastTime
        {
            get
            {
                string temp = "";
                if ((DateTime.Now - DateCreated).Minutes >= 1 && (DateTime.Now - DateCreated).Hours < 1)
                    temp = (DateTime.Now - DateCreated).Minutes.ToString() + " phút" + " " + (DateTime.Now - DateCreated).Seconds.ToString() + " giây";
                else if ((DateTime.Now - DateCreated).Hours >= 1)
                {
                    temp = (DateTime.Now - DateCreated).Hours.ToString() + " giờ" + " " + (DateTime.Now - DateCreated).Minutes.ToString() + " phút" + " " + (DateTime.Now - DateCreated).Seconds.ToString() + " giây";
                }
                else
                {
                    temp = (DateTime.Now - DateCreated).Seconds.ToString() + " giây";
                }
                return temp;
            }
        }

        public int PostID { get; set; }
        public virtual Post Posts{get;set;}
    }
}