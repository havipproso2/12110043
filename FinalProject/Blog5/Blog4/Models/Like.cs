﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog4.Models
{
    public class Like
    {
        [Key]
        [Column(Order = 0)]
        public int PostID { get; set; }
        [Key]
        [Column(Order = 1)]
        public int UserProfileUserID { get; set; }


        public virtual Post Post { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}