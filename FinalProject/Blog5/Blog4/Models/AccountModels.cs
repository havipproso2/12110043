﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace Blog4.Models
{
    public class BlogContext : DbContext
    {
        public BlogContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Level> Levels { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Rate> Rates { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Post>().HasMany(a => a.Tags).WithMany(s => s.Posts).Map(d =>
                {
                    d.MapLeftKey("PostID");
                    d.MapRightKey("TagID");
                    d.ToTable("Post_Tag");
                });
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>().HasMany(a => a.Categorys).WithMany(s => s.Posts).Map(d =>
            {
                d.MapLeftKey("PostID");
                d.MapRightKey("CategoryID");
                d.ToTable("Post_Category");
            });
            base.OnModelCreating(modelBuilder);
        }


        public DbSet<Like> Likes { get; set; }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Hoten { get; set; }
        public DateTime NgayTao { get; set; }
        public string Email { get; set; }
        public string HinhAnh { get; set; }
        public string SobaiDang { get; set; }
        public string SoLuotThich { get; set; }
        public string SoLuotXem { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        
        public string Hoten { get; set; }
        public DateTime NgayTao { get; set; }
        public string Email { get; set; }
        public string HinhAnh { get; set; }
        public string SobaiDang { get; set; }
        public string SoLuotThich { get; set; }
        public string SoLuotXem { get; set; }

        public int LevelID { get; set; }
        public virtual Level Level { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
