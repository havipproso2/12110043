namespace Blog4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Round7 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Views", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Views", "UserProfileUserID", "dbo.UserProfile");
            DropIndex("dbo.Views", new[] { "PostID" });
            DropIndex("dbo.Views", new[] { "UserProfileUserID" });
            AddColumn("dbo.UserProfile", "Hoten", c => c.String());
            AddColumn("dbo.UserProfile", "NgayTao", c => c.DateTime(nullable: false));
            AddColumn("dbo.UserProfile", "Email", c => c.String());
            AddColumn("dbo.UserProfile", "HinhAnh", c => c.String());
            AddColumn("dbo.UserProfile", "SobaiDang", c => c.String());
            AddColumn("dbo.UserProfile", "SoLuotThich", c => c.String());
            AddColumn("dbo.UserProfile", "SoLuotXem", c => c.String());
            AddColumn("dbo.Posts", "View", c => c.Int(nullable: false));
            DropColumn("dbo.Levels", "UserProfileUserId");
            DropTable("dbo.Views");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Views",
                c => new
                    {
                        PostID = c.Int(nullable: false),
                        UserProfileUserID = c.Int(nullable: false),
                        Diem = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostID, t.UserProfileUserID });
            
            AddColumn("dbo.Levels", "UserProfileUserId", c => c.Int(nullable: false));
            DropColumn("dbo.Posts", "View");
            DropColumn("dbo.UserProfile", "SoLuotXem");
            DropColumn("dbo.UserProfile", "SoLuotThich");
            DropColumn("dbo.UserProfile", "SobaiDang");
            DropColumn("dbo.UserProfile", "HinhAnh");
            DropColumn("dbo.UserProfile", "Email");
            DropColumn("dbo.UserProfile", "NgayTao");
            DropColumn("dbo.UserProfile", "Hoten");
            CreateIndex("dbo.Views", "UserProfileUserID");
            CreateIndex("dbo.Views", "PostID");
            AddForeignKey("dbo.Views", "UserProfileUserID", "dbo.UserProfile", "UserId", cascadeDelete: true);
            AddForeignKey("dbo.Views", "PostID", "dbo.Posts", "ID", cascadeDelete: true);
        }
    }
}
