namespace Blog4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Round6 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Views",
                c => new
                    {
                        PostID = c.Int(nullable: false),
                        UserProfileUserID = c.Int(nullable: false),
                        Diem = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostID, t.UserProfileUserID })
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserID, cascadeDelete: false)
                .Index(t => t.PostID)
                .Index(t => t.UserProfileUserID);
            
            CreateTable(
                "dbo.Likes",
                c => new
                    {
                        PostID = c.Int(nullable: false),
                        UserProfileUserID = c.Int(nullable: false),
                        Diem = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostID, t.UserProfileUserID })
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserID, cascadeDelete: false)
                .Index(t => t.PostID)
                .Index(t => t.UserProfileUserID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Likes", new[] { "UserProfileUserID" });
            DropIndex("dbo.Likes", new[] { "PostID" });
            DropIndex("dbo.Views", new[] { "UserProfileUserID" });
            DropIndex("dbo.Views", new[] { "PostID" });
            DropForeignKey("dbo.Likes", "UserProfileUserID", "dbo.UserProfile");
            DropForeignKey("dbo.Likes", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Views", "UserProfileUserID", "dbo.UserProfile");
            DropForeignKey("dbo.Views", "PostID", "dbo.Posts");
            DropTable("dbo.Likes");
            DropTable("dbo.Views");
        }
    }
}
