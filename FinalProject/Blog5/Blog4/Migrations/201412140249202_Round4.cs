namespace Blog4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Round4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryID = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                    })
                .PrimaryKey(t => t.CategoryID);
            
            CreateTable(
                "dbo.Levels",
                c => new
                    {
                        LevelID = c.Int(nullable: false, identity: true),
                        TenCapBac = c.String(),
                        SoBaiDang = c.Int(nullable: false),
                        HinhAnh = c.String(),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.LevelID);
            
            CreateTable(
                "dbo.Rates",
                c => new
                    {
                        PostId = c.Int(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                        Diem = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostId, t.UserProfileUserId })
                .ForeignKey("dbo.Posts", t => t.PostId, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: false)
                .Index(t => t.PostId)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.Post_Category",
                c => new
                    {
                        PostID = c.Int(nullable: false),
                        CategoryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostID, t.CategoryID })
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.CategoryID, cascadeDelete: true)
                .Index(t => t.PostID)
                .Index(t => t.CategoryID);
            
            AddColumn("dbo.UserProfile", "Level_LevelID", c => c.Int());
            AddForeignKey("dbo.UserProfile", "Level_LevelID", "dbo.Levels", "LevelID");
            CreateIndex("dbo.UserProfile", "Level_LevelID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Post_Category", new[] { "CategoryID" });
            DropIndex("dbo.Post_Category", new[] { "PostID" });
            DropIndex("dbo.Rates", new[] { "UserProfileUserId" });
            DropIndex("dbo.Rates", new[] { "PostId" });
            DropIndex("dbo.UserProfile", new[] { "Level_LevelID" });
            DropForeignKey("dbo.Post_Category", "CategoryID", "dbo.Categories");
            DropForeignKey("dbo.Post_Category", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Rates", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.Rates", "PostId", "dbo.Posts");
            DropForeignKey("dbo.UserProfile", "Level_LevelID", "dbo.Levels");
            DropColumn("dbo.UserProfile", "Level_LevelID");
            DropTable("dbo.Post_Category");
            DropTable("dbo.Rates");
            DropTable("dbo.Levels");
            DropTable("dbo.Categories");
        }
    }
}
