namespace Blog4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Round5 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Comments", "Body", c => c.String());
            AlterColumn("dbo.Comments", "Author", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Comments", "Author", c => c.String(nullable: false));
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false));
        }
    }
}
