namespace Blog4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Round71 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Likes", "Diem");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Likes", "Diem", c => c.Int(nullable: false));
        }
    }
}
