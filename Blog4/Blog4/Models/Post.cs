﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog4.Models
{
    public class Post
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required(ErrorMessage = "Tên bài đăng không được để trống!")]
        [StringLength(500, ErrorMessage = "Độ dài ký tự phải từ 20 đến 500!!!", MinimumLength = 20)]
        public String Title { get; set; }
        [Required(ErrorMessage = "Bài viết không được để trống!")]
        [StringLength(int.MaxValue, ErrorMessage = "Bài viết phải có ít nhất 50 kí tự!", MinimumLength = 50)]
        public String Body { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống")]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập đúng kiểu ngày tháng!")]
        public DateTime DateCreated { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống!")]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập đúng kiểu ngày tháng!")]
        public DateTime DateUpdated { get; set; }

        public int UserProfileUserID { get; set; }
        public virtual UserProfile UserProfile { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
    }
}